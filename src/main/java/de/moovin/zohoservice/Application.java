package de.moovin.zohoservice;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.joda.time.DateTimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ErrorMvcAutoConfiguration.class})
@ComponentScan(basePackages = {"de.moovin"})
//@EnableMongoRepositories(basePackages = {"de.moovin"})
public class Application {

  @PostConstruct
  void started() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    DateTimeZone.setDefault(DateTimeZone.UTC);
  }

  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
