#!/bin/bash
# git
git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
git config --global user.name "moovin CI"
git config --global user.email "ci@moovin.de"