
STAGE=production

SERVICE_NAME=`eval "mvn help:evaluate -Dexpression=project.artifactId | grep \"^[^\[]\"" | tail -1`

DOCKER_REGISTRY_PATH=`eval "mvn help:evaluate -Dexpression=docker.image.prefix | grep \"^[^\[]\"" | tail -1`
#DOCKER_REGISTRY=$(DOCKER_REGISTRY_PATH)/$(SERVICE_NAME)



TASK_DEFINITION=ecs_task_definitions/ecs_task_definition_${STAGE}.json
TASK_TMP=task_tmp.json

VERSION=`mvn help:evaluate -Dexpression=project.version | grep -e '^[^\[]' | tail -1`
echo "--Service: "$SERVICE_NAME


echo "-- Create Task Definition Revision"
cp $TASK_DEFINITION $TASK_TMP
sed -i "" -e "s|\${container-image}|$DOCKER_REGISTRY_PATH:$VERSION|g" $TASK_TMP
sed -i "" -e "s|\${service-name}|$SERVICE_NAME|g" $TASK_TMP
aws ecs register-task-definition --cli-input-json file://$TASK_TMP --profile moovin
#cat $TASK_TMP

rm -f $TASK_TMP

